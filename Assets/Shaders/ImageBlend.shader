﻿Shader "Hidden/ImageBlend"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Overlay("Overlay", 2D) = "white" {}
		_AlphaMap("AlphaMap", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		// Cull Off ZWrite Off ZTest Always

		Pass
		{
			ZTest Always Cull Off ZWrite Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment fragAlphaBlend
			
			#include "UnityCG.cginc"
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv  : TEXCOORD0;
				float2 uvo : TEXCOORD1; // overlay
			};

			sampler2D _Overlay;
			sampler2D _AlphaMap;

			sampler2D _MainTex;
			half4 _MainTex_ST;

			half _Intensity;

			v2f vert(appdata_img v) {
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);

				o.uv = UnityStereoScreenSpaceUVAdjust(MultiplyUV(UNITY_MATRIX_TEXTURE0, v.texcoord), _MainTex_ST);
				o.uvo = v.texcoord.xy;

				return o;
			}

			half4 fragAlphaBlend(v2f i) : SV_Target
			{
				// Read colors from textures
				half4 originalColor = tex2D(_MainTex, i.uv);
				half4 blendColor = tex2D(_Overlay, i.uvo);
				half4 alphaRGB = tex2D(_AlphaMap, i.uvo);
				// check
				float check = Luminance(blendColor.rgb);
				blendColor = lerp(blendColor, originalColor, check);
				// Read alpha from map				
				half alpha = Luminance(alphaRGB);
				// Set color by lum
				blendColor = blendColor * (1.0f - alpha);
				// Lerp by intensity
				half4 lerpCol = lerp(originalColor, blendColor, (_Intensity * 0.3f));
				// Return
				return lerpCol;
			}
			ENDCG
		}
	}

	Fallback off
}
