using System;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (NavMeshAgent))]
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class AICharacterControl : MonoBehaviour
    {
        #region Fields
        /// <summary>
        /// The target to aim for
        /// </summary>
        public Transform target;
        /// <summary>
        /// The destination to go to. Only used, when no target is set.
        /// </summary>
        public Vector3? destination;
        #endregion // Fields

        #region Properties
        /// <summary>
        /// the navmesh agent required for the path finding
        /// </summary>
        public NavMeshAgent agent { get; private set; }
        /// <summary>
        /// the character we are controlling
        /// </summary>
        public ThirdPersonCharacter character { get; private set; }

        /// <summary>
        /// True, when the destination was reached
        /// </summary>
        public bool destinationReached { get; private set; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Called on initialization
        /// </summary>
        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();

	        agent.updateRotation = false;
	        agent.updatePosition = true;
        }

        /// <summary>
        /// Called every frame
        /// </summary>
        private void Update()
        {
            // If we are chasing a target, update the destination
            if (target != null)
            {
                agent.SetDestination(target.position);
            }

            // Only check, if the path isn't pending
            if (!agent.pathPending)
            {
                // Check, if the destination was reached
                destinationReached = (agent.remainingDistance <= agent.stoppingDistance);
            }            
            
            // Check if the destination was reached or not
            if (!destinationReached)
            { // If not, move the character
                character.Move(agent.desiredVelocity, false, false);
            }
            else
            { // If so, the character does not need to move
                character.Move(Vector3.zero, false, false);
            }
        }

        /// <summary>
        /// Sets a target transform that should be chased
        /// </summary>
        /// <param name="target">The target to chase</param>
        public void SetTarget(Transform target)
        {
            this.destination = null;
            this.target = target;
        }

        /// <summary>
        /// Sets the destination for the character
        /// </summary>
        /// <param name="destination">The new destination.</param>
        public void SetDestination(Vector3 destination)
        {
            this.target = null;
            this.destination = destination;
            this.agent.SetDestination(destination);
            this.destinationReached = false;
        }
        #endregion // Methods
    }
}
