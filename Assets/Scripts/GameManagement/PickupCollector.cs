﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Master.Special;

public class PickupCollector : MonoBehaviour
{
    bool pickupsOnStartup = false;

    void Start()
    {
        // Find all pickups
        var allPickups = GameObject.FindObjectsOfType<Pickup>();

        pickupsOnStartup = (allPickups.Length > 0);
    }	

	// Update is called once per frame
	void Update ()
    {
        // If no pickups existed on startup, just do nothing
        if (!pickupsOnStartup)
        {
            return;
        }

        // Find all pickups
        var allPickups = GameObject.FindObjectsOfType<Pickup>();

        // If no one is there, reload the scene
        if (allPickups.Length == 0)
        {
            ReloadScene();
        }
	}

    void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetSceneAt(0).name);
    }
}
