﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

namespace Master.GameManagement
{
    public class AudioMixerControl : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private AudioMixerSnapshot m_normalEnvironment;
        [SerializeField]
        private AudioMixerSnapshot m_normalSoundFX;

        [SerializeField]
        private AudioMixerSnapshot m_lowHealthEnvironment;
        [SerializeField]
        private AudioMixerSnapshot m_lowHealthSoundFX;

        public float m_transitionTime = 1.0f;
        #endregion // Fields

        #region Methods
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void MoveToLowHealth()
        {
            m_lowHealthEnvironment.TransitionTo(m_transitionTime);
            m_lowHealthSoundFX.TransitionTo(m_transitionTime);
        }

        public void MoveToNormal()
        {
            m_normalEnvironment.TransitionTo(m_transitionTime);
            m_normalSoundFX.TransitionTo(m_transitionTime);
        }
        #endregion // Methods
    }
}