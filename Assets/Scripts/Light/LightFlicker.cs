﻿using UnityEngine;
using System.Collections;

namespace Master.Lighting
{
    public class LightFlicker : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private Light m_light;
        [SerializeField]
        private Renderer m_renderer;

        private float m_minLightIntensity = 0f;
        private float m_maxLightIntensity = -1f;
        private float m_intensityCoefficient;

        private bool m_flicker = false;

        //private float m_minEmission = 0f;
        //private float m_maxEmision = -1f;
        //private float m_emissionCoefficient;
        #endregion // Fields

        #region Methods
        // Use this for initialization
        void Start()
        {
            /*
            if (m_renderer && m_renderer.material && m_maxEmision < 0f)
            {
                m_maxEmision = m_renderer.material.GetFloat("_EmissionColor"); //_EmissionScale
            }
            */

            if (m_light && m_maxLightIntensity < 0f)
            {
                m_maxLightIntensity = m_light.intensity;
            }

            // Calculate coefficients
            m_intensityCoefficient = m_maxLightIntensity - m_minLightIntensity;
            //m_emissionCoefficient = m_maxEmision - m_minEmission;
        }

        // Update is called once per frame
        void Update()
        {
            Flicker();
        }

        public void Flicker()
        {
            if (m_flicker)
            { // Continue flickering
                // Get a random value
                var lerpFactor = Random.value;
                var lightFactor = Random.value;

                // Calculate intensity and emission
                var lightIntensity = m_minLightIntensity + (lightFactor * m_intensityCoefficient);
                //var emission = m_minEmission + (lightFactor * m_emissionCoefficient);

                // Randomly lerp the light
                m_light.intensity = Mathf.Lerp(m_light.intensity, lightIntensity, lerpFactor);
                //m_renderer.material.SetFloat("_EmissionColor", Mathf.Lerp(m_minEmission, emission, lerpFactor));
            }
            else
            { // Stop flickering
                m_light.intensity = m_maxLightIntensity;
            }
        }

        public void StartAction()
        {
            m_flicker = true;
        }

        public void StopAction()
        {
            m_flicker = false;
        }
        #endregion // Methods
    }
}