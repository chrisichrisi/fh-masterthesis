﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Master.Rendering.ImageEffects;
using Master.GameManagement;
using UnityEngine.UI;

namespace Master.Characters.FirstPerson
{
    [RequireComponent(typeof(AudioSource))]
    public class Healthbar : MonoBehaviour
    {
        #region Fields
        [SerializeField] [Range(0, 10)]
        private int m_health = 10;
        [SerializeField] [Range(0, 10)]
        private int m_maxHealth = 10;
        private float m_healthRatio;

        /// <summary>
        /// Indicates whether the player was hit in this frame or not
        /// </summary>
        private bool m_hit = false;

        #region Audio
        /// <summary>
        /// An Array of heart beat sounds
        /// </summary>
        [SerializeField]
        private AudioClip[] m_heartBeats;
        
        /// <summary>
        /// The audio source
        /// </summary>
        private AudioSource m_AudioSource;

        [SerializeField]
        private AudioMixerControl m_audioMixer;
        #endregion // Audio

        #region View
        /// <summary>
        /// An image reference that can be used to show a hit indicator
        /// </summary>
        [SerializeField]
        private Image m_hitImage;
        private Grayscale m_grayscale;
        private ImageBlend m_bloodSplatters;

        /// <summary>
        /// The color that the hit indicator image should get
        /// </summary>
        [SerializeField]
        private Color m_hitColor;
        /// <summary>
        /// Indicates how fast the hit image is invisible again
        /// </summary>
        [SerializeField]
        private float m_hitFlashSpeed = 2.0f;
        #endregion // View
        #endregion // Fields

        #region Methods
        /// <summary>
        /// Use this for initialization
        /// </summary>
        void Start()
        {
            // Get the required components
            m_AudioSource = GetComponent<AudioSource>();
            m_grayscale = transform.parent.gameObject.GetComponentInChildren<Grayscale>();
            m_bloodSplatters = transform.parent.gameObject.GetComponentInChildren<ImageBlend>();

            //
            //m_hitColor = m_hitImage.color;
            m_hitImage.color = Color.clear; // Clear the hit image
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        void Update()
        {
            // First update the new health stat
            UpdateHealth();
            // Now update all the parts that relate to the new health stat
            UpdateHeartbeat();
            UpdateView();

            // Finally set the hit info to false
            m_hit = false;
        }

        #region Update Health
        /// <summary>
        /// Updates the health
        /// </summary>
        void UpdateHealth()
        {
            // Calculate new health ratio
            m_healthRatio = m_health / (float)m_maxHealth;
        }

        /// <summary>
        /// Applies the given amount of damage
        /// </summary>
        /// <param name="damage">The damage to apply</param>
        void ApplyDamage(int damage)
        {
            // Reduce health by damage, but don't go below zero
            m_health = m_health - damage;
            m_hit = true;

            // Reload scene if health dropped below zero
            if (m_health <= 0)
            {
                SceneManager.LoadScene(SceneManager.GetSceneAt(0).name);
            }
        }
        #endregion // Update Health

        #region Update Heartbeat
        /// <summary>
        /// Updates the heartbeat audio
        /// </summary>
        void UpdateHeartbeat()
        {
            if (m_hit == true)
            {
                // Transition Audio mixers if needed
                if (m_health < 5)
                {
                    m_audioMixer.MoveToLowHealth();
                }
                else
                {
                    m_audioMixer.MoveToNormal();
                }
            }

            // Now have a look about the audio clip to use
            AudioClip newHeartbeatClip;
            if (!CheckHeartbeat(out newHeartbeatClip))
            { // Heartbeat should not be changed
                return;
            }

            // Stop looping
            m_AudioSource.loop = false;

            if (m_AudioSource.isPlaying)
            { // The old heartbeat is still playing, so don't do anything yet
                return;
            }

            // Set the new clip and start looping again
            m_AudioSource.clip = newHeartbeatClip;
            m_AudioSource.loop = true;
            m_AudioSource.Play();
        }

        /// <summary>
        /// Checks, if the heartbeat audio needs to be updated
        /// </summary>
        /// <param name="newAudioClip">New heartbeat audio clip that should be played</param>
        /// <returns>True, if the heartbeat audio needs to be updated</returns>
        bool CheckHeartbeat(out AudioClip newAudioClip)
        {
            int highestIndex = m_heartBeats.Length - 1;
            int heartbeatIndex = Mathf.Clamp((int)(m_healthRatio * m_heartBeats.Length), 0, highestIndex);

            // Inverse the resulting index
            heartbeatIndex = highestIndex - heartbeatIndex;

            // Get the new clip from the array
            var newClip = m_heartBeats[heartbeatIndex];

            if (m_AudioSource.clip == newClip)
            { // still playing the same clip
                newAudioClip = null;
                return false;
            }

            newAudioClip = newClip;
            return true;
        }
        #endregion // Update Heartbeat

        #region Update View
        /// <summary>
        /// Updates all view specific stuff for the health
        /// </summary>
        void UpdateView()
        {
            m_grayscale.intensity = (1.0f - m_healthRatio);
            m_bloodSplatters.m_intensity = (1.0f - m_healthRatio);
            UpdateHitImage();
        }

        /// <summary>
        /// Updates the hit indicator image
        /// </summary>
        void UpdateHitImage()
        {
            if (m_hit)
            {
                m_hitImage.color = m_hitColor;
            }
            else
            {
                m_hitImage.color = Color.Lerp(m_hitImage.color, Color.clear, m_hitFlashSpeed * Time.deltaTime);
            }
        }
        #endregion // Update View
        #endregion // Methods
    }
}