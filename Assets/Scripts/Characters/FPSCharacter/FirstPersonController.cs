using System;
using UnityEngine;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;

namespace UnityStandardAssets.Characters.FirstPerson
{
    public enum MovingState
    {
        Sneak,
        Walk,
        Run
    }

    [RequireComponent(typeof (CharacterController))]
    [RequireComponent(typeof (AudioSource))]
    public class FirstPersonController : MonoBehaviour
    {
        #region Fields
        [SerializeField] public MovingState m_movingState;
        // Speed Variables
        [SerializeField] private float m_WalkSpeed;
        [SerializeField] private float m_RunSpeed;
        [SerializeField] private float m_SneakSpeed;
        // Step Variables
        [SerializeField] [Range(0f, 1f)] private float m_RunstepLenghten;
        [SerializeField] [Range(1f, 2f)] private float m_SneakstepLenghten;
        [SerializeField] private float m_StepInterval;
        // Other stuff
        [SerializeField] private float m_JumpSpeed;
        [SerializeField] private float m_StickToGroundForce;
        [SerializeField] private float m_GravityMultiplier;
        [SerializeField] private MouseLook m_MouseLook;
        // FOVKicks
        [SerializeField] private bool m_UseFovKick;
        [SerializeField] private FOVKick m_FovKick = new FOVKick();
        // HeadBob variables
        [SerializeField] private bool m_UseHeadBob;
        [SerializeField] private CurveControlledBob m_HeadBob = new CurveControlledBob();
        [SerializeField] private LerpControlledBob m_JumpBob = new LerpControlledBob();
        // AudioClips
        [SerializeField] private AudioClip[] m_FootstepSounds;    // an array of footstep sounds that will be randomly selected from.
        [SerializeField] private AudioClip m_JumpSound;           // the sound played when character leaves the ground.
        [SerializeField] private AudioClip m_LandSound;           // the sound played when character touches back on ground.

        private Camera m_Camera;
        private bool m_Jump;
        private Vector2 m_Input;
        public Vector3 m_MoveDir = Vector3.zero;
        private CharacterController m_CharacterController;
        private CollisionFlags m_CollisionFlags;
        private bool m_PreviouslyGrounded;
        private Vector3 m_OriginalCameraPosition;
        private float m_StepCycle;
        private float m_NextStep;
        private bool m_Jumping;
        private AudioSource m_AudioSource;

        #region Sneaking-Control
        /// <summary>
        /// Transform that should be 
        /// </summary>
        [SerializeField]
        private Transform m_sneakTransform;

        private float m_heightNormal;
        [SerializeField]
        private float m_heightSneaking = 0.5f;
        #endregion // Sneaking-Control
        #endregion // Fields

        #region Methods
        /// <summary>
        /// Called on initialization
        /// </summary>
        private void Start()
        {
            m_CharacterController = GetComponent<CharacterController>();
            m_Camera = Camera.main;
            m_OriginalCameraPosition = m_Camera.transform.localPosition;
            m_FovKick.Setup(m_Camera);
            m_HeadBob.Setup(m_Camera, m_StepInterval);
            m_StepCycle = 0f;
            m_NextStep = m_StepCycle/2f;
            m_Jumping = false;
            m_AudioSource = GetComponent<AudioSource>();
			m_MouseLook.Init(transform , m_Camera.transform);

            // Get the sneak transform (parent of main camera)
            m_sneakTransform = m_Camera.transform.parent.transform;

            // Get the default position
            m_heightNormal = m_sneakTransform.localPosition.y;
        }


        // Update is called once per frame
        private void Update()
        {
            RotateView();

            // Store old moving state
            var oldMovingState = m_movingState;
            // Get new moving state
            m_movingState = GetNewMovingState(oldMovingState);

            // Update height
            UpdateHeight(oldMovingState, m_movingState);

            if (!m_PreviouslyGrounded && m_CharacterController.isGrounded)
            {
                StartCoroutine(m_JumpBob.DoBobCycle());
                PlayLandingSound();
                m_MoveDir.y = 0f;
                m_Jumping = false;
            }
            if (!m_CharacterController.isGrounded && !m_Jumping && m_PreviouslyGrounded)
            {
                m_MoveDir.y = 0f;
            }

            m_PreviouslyGrounded = m_CharacterController.isGrounded;
        }


        private void PlayLandingSound()
        {
            m_AudioSource.clip = m_LandSound;
            m_AudioSource.Play();
            m_NextStep = m_StepCycle + .5f;
        }


        private void FixedUpdate()
        {
            float speed;
            GetInput(out speed);
            // always move along the camera forward as it is the direction that it being aimed at
            Vector3 desiredMove = transform.forward * m_Input.y + transform.right * m_Input.x;

            // get a normal for the surface that is being touched to move along it
            RaycastHit hitInfo;
            Physics.SphereCast(transform.position, m_CharacterController.radius, Vector3.down, out hitInfo,
                               m_CharacterController.height/2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
            desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

            m_MoveDir.x = desiredMove.x * speed;
            m_MoveDir.z = desiredMove.z * speed;


            if (m_CharacterController.isGrounded)
            {
                m_MoveDir.y = -m_StickToGroundForce;

                if (m_Jump)
                {
                    m_MoveDir.y = m_JumpSpeed;
                    PlayJumpSound();
                    m_Jump = false;
                    m_Jumping = true;
                }
            }
            else
            {
                m_MoveDir += Physics.gravity*m_GravityMultiplier*Time.fixedDeltaTime;
            }
            m_CollisionFlags = m_CharacterController.Move(m_MoveDir*Time.fixedDeltaTime);

            ProgressStepCycle(speed);
            UpdateCameraPosition(speed);

            m_MouseLook.UpdateCursorLock();
        }


        private void PlayJumpSound()
        {
            m_AudioSource.clip = m_JumpSound;
            m_AudioSource.Play();
        }


        private void ProgressStepCycle(float speed)
        {
            if (m_CharacterController.velocity.sqrMagnitude > 0 && (m_Input.x != 0 || m_Input.y != 0))
            {
                m_StepCycle += (m_CharacterController.velocity.magnitude + (speed * GetStepLength(m_movingState))) * Time.fixedDeltaTime;
            }

            if (!(m_StepCycle > m_NextStep))
            {
                return;
            }

            m_NextStep = m_StepCycle + m_StepInterval;

            PlayFootStepAudio();
        }


        private void PlayFootStepAudio()
        {
            if (!m_CharacterController.isGrounded)
            {
                return;
            }

            if (m_movingState == MovingState.Sneak)
            {
                m_AudioSource.volume = 0.4f;
            }
            else
            {
                m_AudioSource.volume = 1.0f;
            }
            // pick & play a random footstep sound from the array,
            // excluding sound at index 0
            int n = Random.Range(1, m_FootstepSounds.Length);
            m_AudioSource.clip = m_FootstepSounds[n];
            m_AudioSource.PlayOneShot(m_AudioSource.clip);
            // move picked sound to index 0 so it's not picked next time
            m_FootstepSounds[n] = m_FootstepSounds[0];
            m_FootstepSounds[0] = m_AudioSource.clip;
        }


        private void UpdateCameraPosition(float speed)
        {
            Vector3 newCameraPosition;
            if (!m_UseHeadBob)
            {
                return;
            }
            if (m_CharacterController.velocity.magnitude > 0 && m_CharacterController.isGrounded)
            {
                m_Camera.transform.localPosition =
                    m_HeadBob.DoHeadBob(m_CharacterController.velocity.magnitude + (speed * GetStepLength(m_movingState)));
                newCameraPosition = m_Camera.transform.localPosition;
                newCameraPosition.y = m_Camera.transform.localPosition.y - m_JumpBob.Offset();
            }
            else
            {
                newCameraPosition = m_Camera.transform.localPosition;
                newCameraPosition.y = m_OriginalCameraPosition.y - m_JumpBob.Offset();
            }
            m_Camera.transform.localPosition = newCameraPosition;
        }


        private void GetInput(out float speed)
        {
            // Read input
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            // set the desired speed to be walking or running
            speed = GetMovingSpeed(m_movingState);
            m_Input = new Vector2(horizontal, vertical);

            // normalize input if it exceeds 1 in combined length:
            if (m_Input.sqrMagnitude > 1)
            {
                m_Input.Normalize();
            }

            /*
            // handle speed change to give an fov kick
            // only if the player is going to a run, is running and the fovkick is to be used
            if (isWalking != waswalking && m_UseFovKick && m_CharacterController.velocity.sqrMagnitude > 0)
            {
                StopAllCoroutines();
                StartCoroutine(!isWalking ? m_FovKick.FOVKickUp() : m_FovKick.FOVKickDown());
            }
            */
        }


        private void RotateView()
        {
            // If the flashlight modifier button is pressed, don't update the mouse look
            if (Input.GetButton("FlashlightModifier")) { return; }

            m_MouseLook.LookRotation (transform, m_Camera.transform);
        }


        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            Rigidbody body = hit.collider.attachedRigidbody;
            //dont move the rigidbody if the character is on top of it
            if (m_CollisionFlags == CollisionFlags.Below)
            {
                return;
            }

            if (body == null || body.isKinematic)
            {
                return;
            }
            body.AddForceAtPosition(m_CharacterController.velocity*0.1f, hit.point, ForceMode.Impulse);
        }
        
        /// <summary>
        /// Returns the speed for a given moving state
        /// </summary>
        /// <param name="movingState">The moving state for which the speed should be get</param>
        /// <returns>The moving speed for a given state</returns>
        private float GetMovingSpeed(MovingState movingState)
        {
            if (movingState == MovingState.Run)
            {
                return m_RunSpeed;
            }
            else if (movingState == MovingState.Sneak)
            {
                return m_SneakSpeed;
            }

            return m_WalkSpeed;
        }

        /// <summary>
        /// Returns the step length for a given moving state
        /// </summary>
        /// <param name="movingState">The moving state for which the step length should be get</param>
        /// <returns>The step length for a given moving state</returns>
        private float GetStepLength(MovingState movingState)
        {
            if (movingState == MovingState.Run)
            {
                return m_RunstepLenghten;
            }
            else if (movingState == MovingState.Sneak)
            {
                return m_SneakstepLenghten;
            }

            return 1.0f;
        }

        /// <summary>
        /// Returns the new moving state based on the input and on the old moving state.
        /// </summary>
        /// <param name="oldMovingState">The moving state that was before</param>
        /// <returns>The new moving state based on the input and the old moving state.</returns>
        private MovingState GetNewMovingState(MovingState oldMovingState)
        {
            bool runModifierPressed = Input.GetButton("Run");

            // Check if the sneak modifier was pressed
            if (Input.GetButtonDown("Sneak"))
            { // Sneak Button was pressed
                if (oldMovingState != MovingState.Sneak)
                { // If the player didn't sneak before, the player sneaks nowm
                    return MovingState.Sneak;
                }

                // Otherwise the player should walk.
                return MovingState.Walk;
            }
            else if (oldMovingState == MovingState.Walk && runModifierPressed)
            {
                return MovingState.Run;
            }
            else if (oldMovingState == MovingState.Run && !runModifierPressed)
            {
                return MovingState.Walk;
            }

            // In all other cases, the new moving state is the old moving state
            return oldMovingState;
        }

        #region Sneak-Control
        /// <summary>
        /// Updates the height.
        /// </summary>
        /// <param name="oldState">The moving state at the start of the frame</param>
        /// <param name="newState">The moving state at the end of the frame</param>
        private void UpdateHeight(MovingState oldState, MovingState newState)
        {
            // This height update should only be applied when there is no VR device
            if (UnityEngine.VR.VRDevice.isPresent) { return; }

            float targetHeight = (newState == MovingState.Sneak ? m_heightSneaking : m_heightNormal);
            Vector3 oldPos = m_sneakTransform.localPosition;
            float currentHeight = oldPos.y;

            if (currentHeight != targetHeight)
            {
                float newHeight;
                float heightDiff = Mathf.Abs(currentHeight - targetHeight);

                if (heightDiff < 0.05f)
                {
                    // We are close enough, so just set it
                    newHeight = targetHeight;
                }
                else
                {
                    // Lerp towards
                    newHeight = Mathf.Lerp(currentHeight, targetHeight, Time.deltaTime * 10f);
                }

                m_sneakTransform.localPosition = new Vector3(oldPos.x, newHeight, oldPos.z);
            }
        }

        /// <summary>
        /// Lineary interpolates between sneaking height and normal height by t.
        /// </summary>
        /// <param name="t">A value between 0 and 1 used for interpolation</param>
        public void LerpHeight(float t)
        {
            // Get the old position
            Vector3 oldPos = m_sneakTransform.localPosition;
            // Calculate the lerped height
            oldPos.y = Mathf.Lerp(m_heightSneaking, m_heightNormal, t);
            // Set the new height
            m_sneakTransform.localPosition = oldPos;
        }
        #endregion // Sneak-Control
        #endregion // Methods
    }
}
