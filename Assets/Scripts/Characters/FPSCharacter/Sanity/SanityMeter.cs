﻿using UnityEngine;
using System.Collections;

namespace Master.Character.Sanity
{
    public class SanityMeter : MonoBehaviour
    {
        #region Fields
        [SerializeField] [Range(0, 1f)]
        public float m_sanity = -1f;

        private float m_maxSanity = 1f;
        private float m_minSanity = 0f;

        #region Sanity-Regeneration
        [SerializeField]
        private float m_timeBeforeRegenerationStarts = 10f;
        private float m_sanityRegenerationTimer = 0f;
        /// <summary>
        /// The maximum value to which sanity regenerates
        /// </summary>
        private float m_maxSanityRegeneration = 0.5f;
        #endregion // Sanity-Regeneration
        #endregion // Fields

        #region Methods
        /// <summary>
        /// Use this for initialization
        /// </summary>
        void Start()
        {
            // Set sanity to max, if it would be below zero
            if (m_sanity < 0f) { m_sanity = m_maxSanity; }
        }
        
        /// <summary>
        /// Update is called once per frame
        /// </summary>
        void Update()
        {
            RegenerateSanity();
        }

        #region Sanity-Regeneration
        /// <summary>
        /// Regenerates sanity under certain conditions
        /// </summary>
        void RegenerateSanity()
        {
            // No need to regenerate
            if (m_sanity > m_maxSanityRegeneration) { return; }

            // First check, if the timer is still running
            if (m_sanityRegenerationTimer > 0f)
            { // If so, decrease and do nothing further
                m_sanityRegenerationTimer -= Time.deltaTime;
                return;
            }

            // Regenerate sanity
            m_sanity = Mathf.Lerp(m_sanity, m_maxSanityRegeneration, Time.deltaTime * 0.1f);
        }
        #endregion // Sanity-Regeneration

        #region Sanity-Manipulation
        /// <summary>
        /// Decreases sanity by damage.
        /// </summary>
        /// <param name="damage">The sanity damage</param>
        public void DecreaseSanity(float damage)
        {
            m_sanity = Mathf.Max(0f, m_sanity - damage);
            m_sanityRegenerationTimer = m_timeBeforeRegenerationStarts;
        }
        #endregion // Sanity-Manipulation
        #endregion // Methods
    }
}