﻿using UnityEngine;
using System.Collections;

namespace Master.Character.Sanity
{
    [RequireComponent(typeof(AudioSource))]
    public class Whispering : MonoBehaviour
    {
        #region Fields
        private SanityMeter m_sanityMeter;

        [SerializeField] [Range(0, 1f)]
        public float m_sanityStartLevel = 0.5f;

        #region Audio
        private AudioSource m_audio;

        [SerializeField]
        public AudioClip m_audioWhispering;
        #endregion // Audio
        #endregion // Fields

        #region Methods
        /// <summary>
        /// Use this for initialization
        /// </summary>
        void Start()
        {
            m_sanityMeter = GetComponentInParent<SanityMeter>();
            m_audio = GetComponent<AudioSource>();
            m_audio.clip = m_audioWhispering;
            m_audio.Play();
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        void Update()
        {
            float currentSanity = m_sanityMeter.m_sanity;
            UpdateVolume(currentSanity);
        }

        void UpdateVolume(float currentSanity)
        {
            if (currentSanity > m_sanityStartLevel)
            { // Reset audio
                m_audio.volume = Mathf.Lerp(m_audio.volume, 0f, Time.deltaTime);
                return;
            }

            float x = (currentSanity) / (m_sanityStartLevel);
            m_audio.volume = 1f - x;

            //m_audio.volume = Mathf.Lerp(m_audio.volume, volume, Time.deltaTime);
        }
        #endregion // Methods
    }
}