﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

namespace Master.Character.Sanity
{
    public class InsanityVisualization : MonoBehaviour
    {
        #region Fields
        private SanityMeter m_sanityMeter;

        #region Twirl
        private Twirl m_twirl;

        [SerializeField]
        private float m_twirlMaximumSanity = 0.25f;
        [SerializeField]
        private float m_twirlMaxAngel = 90f;
        #endregion // Twirl
        #endregion // Fields

        #region Methods
        /// <summary>
        /// Use this for initialization
        /// </summary>
        void Start()
        {
            // Get the sanity meter
            m_sanityMeter = GetComponent<SanityMeter>();

            // Get main camera
            var cam = Camera.main;
            var camGO = cam.gameObject;

            // Get image effects
            m_twirl = camGO.GetComponent<Twirl>();
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        void Update()
        {
            UpdateTwirl();
        }

        #region Twirl
        /// <summary>
        /// Updates the twirl effect
        /// </summary>
        private void UpdateTwirl()
        {
            // Get the current angel
            var currentAngel = m_twirl.angle;

            // Calculate the target angel
            float targetAngel = CalculateTwirlTargetAngel(currentAngel);

            Debug.Log("currentAngel: " + currentAngel);
            Debug.Log("targetAngel: " + targetAngel);

            // Check if the difference between target and current bigger then a certain threshold
            if (Mathf.Abs(Mathf.Abs(targetAngel) - Mathf.Abs(currentAngel)) > 0.1f)
            {
                // If so, Lerp towards the target angel
                targetAngel = Mathf.Lerp(currentAngel, targetAngel, Time.deltaTime);
            }

            // Set the target angel
            m_twirl.angle = targetAngel;
        }

        /// <summary>
        /// Calculates the angel that should be applied to the twirl effect
        /// </summary>
        /// <param name="currentAngel">The currently applied twirl effect angel</param>
        /// <returns>The angel that should be applied to the twirl effect</returns>
        private float CalculateTwirlTargetAngel(float currentAngel)
        {
            // Get current sanity
            float sanity = m_sanityMeter.m_sanity;

            // If we didn't fall underneath the maximum sanity, there should be no twirl effect
            if (sanity > m_twirlMaximumSanity) { return 0f; }

            // Calculate the intensity
            float intensity = 1f - (sanity / m_twirlMaximumSanity);

            // Calculate the target angel
            float targetAngel = m_twirlMaxAngel * intensity;

            // Calculate the twirl direction and calculate the new target angel
            targetAngel = targetAngel * CalculateTwirlDirection(targetAngel, currentAngel);

            // Finally do a wrap around
            targetAngel = (360f + targetAngel) % 360f;

            return targetAngel;
        }

        /// <summary>
        /// Returns the direction in which the twirl should go
        /// </summary>
        /// <param name="targetAngel">The calculated target angel</param>
        /// <param name="currentAngel">The currently applied angel</param>
        /// <returns>The direction, in which the target angel should be applied. 1 or -1</returns>
        private float CalculateTwirlDirection(float targetAngel, float currentAngel)
        {
            // Target angel wasn't reached, so we still need to go in this direction
            if (targetAngel > currentAngel) { return 1f; }

            return 1f;
        }
        #endregion // Twirl
        #endregion // Methods
    }
}