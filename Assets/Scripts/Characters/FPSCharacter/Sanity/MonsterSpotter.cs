﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Master.Character.Sanity
{
    [RequireComponent(typeof(AudioSource))]
    public class MonsterSpotter : MonoBehaviour
    {
        #region Fields
        private Transform m_cam;
        public LinkedList<Transform> m_spottedMonsters = new LinkedList<Transform>();

        /// <summary>
        /// The value by which the sanity should be decrease
        /// </summary>
        private float m_sanityImpact = 0.075f;
        /// <summary>
        /// The distance that the player can see
        /// </summary>
        private float m_rayDistance = 100f;

        #region Audio
        private AudioSource m_audio;

        [SerializeField]
        private AudioClip m_audioSpotted;
        #endregion // Audio
        #endregion // Fields


        #region Methods
        /// <summary>
        /// Use this for initialization
        /// </summary>
        void Start()
        {
            // Get the main camera
            m_cam = Camera.main.transform;

            m_audio = GetComponent<AudioSource>();
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        void Update()
        {
            // Check if a monster was spotted
            var newMonsterSpotted = MonsterSpotted();

            if (newMonsterSpotted)
            { // If a monster was spotted, decrease the sanity
                m_audio.PlayOneShot(m_audioSpotted);
                gameObject.BroadcastMessage("DecreaseSanity", m_sanityImpact);
            }
        }

        #region Monster-Spotting
        /// <summary>
        /// Checks if a monster was spotted, that wasn't spotted before
        /// </summary>
        /// <returns>True, if a monster was spotted, that wasn't spotted before</returns>
        private bool MonsterSpotted()
        {
            RaycastHit hitInfo;
            if (!Physics.Raycast(m_cam.position, m_cam.forward, out hitInfo, m_rayDistance))
            { // Nothing was hit
                return false;
            }

            Debug.DrawLine(m_cam.position, hitInfo.point);

            // Transform that was hit
            var transform = hitInfo.transform;

            if (transform.tag != "Monster")
            { // No monster was hit
                return false;
            }

            if (m_spottedMonsters.Contains(transform))
            { // Monster was already spotted before
                return false;
            }

            // Add the monster in the front
            m_spottedMonsters.AddFirst(transform);
            return true;
        }
        #endregion // Monster-Spotting
        #endregion // Methods
    }
}