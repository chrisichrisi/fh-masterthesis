﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
using UnityStandardAssets.Characters.ThirdPerson;

namespace Master.Characters.AI
{
    class PlayerCheck
    {
        #region Fields
        public Transform m_player;
        public FirstPersonController m_fpsController;

        public Vector3 m_lastKnownPosition = Vector3.zero;
        #endregion // Fields

        
        internal PlayerCheck(GameObject go_player)
        {
            m_player = go_player.transform;
            m_fpsController = go_player.GetComponent<FirstPersonController>();
        }

        #region Methods
        internal float GetDistance(Vector3 pos) { return Vector3.Distance(pos, m_player.position); }

        internal bool isSneaking() { return (m_fpsController.m_movingState == MovingState.Sneak); }

        internal bool isMoving()
        {
            return ((Mathf.Abs(m_fpsController.m_MoveDir.x) + Mathf.Abs(m_fpsController.m_MoveDir.z)) > 0.1f);
        }

        internal void StoreLastKnownPosition() { m_lastKnownPosition = m_player.position; }
        #endregion // Methods
    }

    public enum BlindMonsterState
    {
        Random,
        Patrol,
        Chase,
        Search
    }


    [RequireComponent(typeof(AICharacterControl))]
    [RequireComponent(typeof(AudioSource))]
    public class BlindMonster : MonoBehaviour
    {
        #region Fields
        private PlayerCheck m_player;
        private AICharacterControl m_aiCharacterController;

        #region Sanity
        private float m_sanityImpact = 0.1f;
        private bool m_didDecreaseSanity = false;
        #endregion // Sanity

        #region Audio
        private AudioSource m_audio;
        [SerializeField]
        private AudioClip m_chasingClip;
        #endregion // Audio

        #region Editor-Fields
        [SerializeField]
        private BlindMonsterState m_state = BlindMonsterState.Random;

        /// <summary>
        /// The range, in which the monster can hear.
        /// </summary>
        [SerializeField]
        private float m_hearingDistance = 10;
        #endregion // Editor Fields

        #region Patrol
        /// <summary>
        /// A collection of way points to patrol
        /// </summary>
        [SerializeField]
        private Vector3[] m_patrolPoints;
        /// <summary>
        /// The number of the currently targetted way point
        /// </summary>
        private int m_currentPatrolPointIndex = 0;
        #endregion // Patrol
        #endregion // Fields

        #region Methods
        /// <summary>
        /// Used for initialization
        /// </summary>
        void Start()
        {
            m_aiCharacterController = GetComponent<AICharacterControl>();
            m_audio = GetComponent<AudioSource>();

            // Now get all the players informations
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            // Initialize the player collection
            m_player = new PlayerCheck(players[0]);

            // Check if patrol points are set
            if (m_patrolPoints == null || m_patrolPoints.Length == 0)
            { // If not, add the start position
                m_patrolPoints = new Vector3[1];
            }
            // Transform the points in world space
            for (int i = 0; i < m_patrolPoints.Length; ++i)
            {
                m_patrolPoints[i] = transform.TransformPoint(m_patrolPoints[i]);
            }
        }

        /// <summary>
        /// Update, called once per frame
        /// </summary>
        void Update()
        {
            UpdateState();

            UpdateTarget();
        }

        /// <summary>
        /// Checks if the monster should chase the player
        /// </summary>
        /// <returns>True, if the monster should chase the player</returns>
        public bool ShouldChasePlayer()
        {
            // If the player is sneaking and this monster is not already chasing
            if (m_player.isSneaking() && m_state != BlindMonsterState.Chase) { return false; }
            // Skip this player, if no movement is applied
            if (!m_player.isMoving()) { return false; }

            // Get the distance
            var distance = m_player.GetDistance(this.transform.position);

            // Skip this player, if the player is not in hearing distance
            if (distance > m_hearingDistance) { return false; }

            // Store the position and return true
            m_player.StoreLastKnownPosition();
            return true;
        }

        /// <summary>
        /// Updates the state of the monster
        /// </summary>
        private void UpdateState()
        {
            // Get the old state
            BlindMonsterState oldState = m_state;

            // Check if the monster should chase the player
            bool shouldChasePlayer = ShouldChasePlayer();

            // Break here, since chasing is the most significant state to be in
            if (shouldChasePlayer)
            {
                m_state = BlindMonsterState.Chase;
                if (!m_didDecreaseSanity)
                {
                    m_audio.PlayOneShot(m_chasingClip);
                    m_player.m_player.BroadcastMessage("DecreaseSanity", m_sanityImpact);
                    m_didDecreaseSanity = true;
                }
                return;
            }

            // No chase is going on at this point

            // Check, if a chase was going on before
            if (oldState == BlindMonsterState.Chase)
            { // If so, the monster should search for the player
                m_state = BlindMonsterState.Search;
                return;
            }

            // The monster finished checking last position => go patrol
            if ((m_state == BlindMonsterState.Search && m_aiCharacterController.destinationReached))
            {
                m_state = BlindMonsterState.Patrol;
                return;
            }
        }

        /// <summary>
        /// Updates the target.
        /// </summary>
        private void UpdateTarget()
        {
            SetTarget(m_state);
        }

        /// <summary>
        /// Sets the target, based on the given state
        /// </summary>
        /// <param name="state">The state for which the target should be set</param>
        public void SetTarget(BlindMonsterState state)
        {
            if (state == BlindMonsterState.Chase)
            { // Go get the player
                m_aiCharacterController.SetTarget(m_player.m_player);
            }
            if (state == BlindMonsterState.Search)
            { // Go to the place the player was last
                m_aiCharacterController.SetDestination(m_player.m_lastKnownPosition);
            }
            if (state == BlindMonsterState.Patrol)
            { // Patrol the various points
                if (m_aiCharacterController.destinationReached)
                { // Calculate new patrol point index
                    m_currentPatrolPointIndex = ((m_currentPatrolPointIndex + 1) % m_patrolPoints.Length);
                    Debug.Log("m_currentPatrolPointIndex: " + m_currentPatrolPointIndex);
                }

                // Set the destination
                m_aiCharacterController.SetDestination(m_patrolPoints[m_currentPatrolPointIndex]);
            }
        }
        #endregion // Methods
    }

}