﻿using UnityEngine;
using System.Collections;

namespace Master.Characters.AI
{
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(ParticleSystem))]
    [RequireComponent(typeof(AudioSource))]
    public class ExplosiveMonster : MonoBehaviour
    {
        #region Fields
        /// <summary>
        /// Specifies the amount of damage it does on exploding
        /// </summary>
        [SerializeField] [Range(1, 3)]
        private int m_damage = 3;
        private float m_sanityImpact = 0.15f;

        [SerializeField]
        private AudioClip m_explodeSound;

        /// <summary>
        /// If true, the destroying has already started
        /// </summary>
        private bool m_isExploding = false;

        #region Components
        private AudioSource m_audioSource;
        private ParticleSystem m_particleSystem;
        private Renderer m_particleSystemRenderer;
        #endregion // Components
        #endregion // Fields

        #region Methods
        /// <summary>
        /// Called on startup
        /// </summary>
        void Start()
        {
            // Get the components
            m_audioSource = GetComponent<AudioSource>();
            m_particleSystem = GetComponent<ParticleSystem>();
            m_particleSystemRenderer = GetComponent<Renderer>();
        }

        /// <summary>
        /// Called every frame
        /// </summary>
        void Update()
        {
            if (!m_isExploding)
            { // Destroying hasn't started yet, so skip update
                return;
            }

            // Destroying has already started...
            if (m_particleSystem.isPlaying)
            { // ...but the particle system is still playing, sooo skip too
                return;
            }

            // ...and the particle system is done playing. Destroy the parent
            DestroyObject(transform.parent.gameObject);
        }

        /// <summary>
        /// Called if something enters the collider
        /// </summary>
        /// <param name="other">The gameobject that entered the collider</param>
        void OnTriggerEnter(Collider other)
        {
            if (m_isExploding)
            { // Don't re-explode
                return;
            }

            // Get the collided game object
            var otherGameObject = other.gameObject;

            // Check if the player entered the collider
            if (otherGameObject.CompareTag("Player"))
            {
                Explode(otherGameObject);
            }
        }

        /// <summary>
        /// Starts exploding
        /// </summary>
        /// <param name="player"></param>
        void Explode(GameObject player)
        {
            m_isExploding = true;
            // If so, explode and apply damage
            HideExceptParticleSystem();
            m_particleSystem.Play();
            m_audioSource.PlayOneShot(m_explodeSound);
            player.BroadcastMessage("ApplyDamage", m_damage);
            player.BroadcastMessage("DecreaseSanity", m_sanityImpact);
        }

        /// <summary>
        /// Hides all visible components of the game object, except the particle system
        /// </summary>
        void HideExceptParticleSystem()
        {
            // Get all render components
            Renderer[] lChildRenderers = transform.parent.gameObject.GetComponentsInChildren<Renderer>();

            // Loop all renderer components
            foreach (Renderer lRenderer in lChildRenderers)
            {
                if (lRenderer != m_particleSystemRenderer)
                { // Disable all, except the particle renderer
                    lRenderer.enabled = false;
                }
            }

            transform.parent.gameObject.GetComponent<Collider>().enabled = false;
        }
        #endregion // Methods
    }
}
