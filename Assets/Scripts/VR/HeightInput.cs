﻿using UnityEngine;
using UnityEngine.VR;
using UnityStandardAssets.Characters.FirstPerson;

namespace Master.VR
{
    public class HeightInput : MonoBehaviour
    {
        #region Fields
        private VRNode m_vrNode = VRNode.Head; // The node to use to query the vr data

        [SerializeField]
        public Vector3 m_upPosition;

        [SerializeField]
        public Vector3 m_downPosition;

        [SerializeField]
        public float m_intensity = 0.03f;

        private FirstPersonController m_fpc;

        private bool m_isUp = true;
        private bool m_isCalibrated = false;
        private float m_heightDifference = 0f;
        #endregion // Fields

        #region Methods
        // Use this for initialization
        void Start()
        {
            // Check if a VR device is present
            if (!VRDevice.isPresent)
            { // If not, disable this script
                enabled = false;
            }

            // get the first person controller
            m_fpc = GetComponentInParent<FirstPersonController>();
        }

        // Update is called once per frame
        void Update()
        {
            Calibrate();

            // Check if the HMD was calibrated
            if (!m_isCalibrated) { return; }

            // Get the current position
            var currentPos = InputTracking.GetLocalPosition(m_vrNode);

            UpdateIsUp(currentPos);
            UpdateHeight(currentPos);
        }

        #region Input
        /// <summary>
        /// Update the internal state of isUp
        /// </summary>
        /// <param name="currentPos">The current position of the vr node</param>
        private void UpdateIsUp(Vector3 currentPos)
        {
            bool isUp = m_isUp;

            if (isUp && ((m_downPosition.y + m_intensity) > currentPos.y))
            {
                isUp = false;
            }
            else if (!isUp && ((m_upPosition.y - m_intensity) < currentPos.y))
            {
                isUp = true;
            }

            if (m_isUp != isUp)
            {
                // Is up changed!
                if (isUp)
                {
                    // We came from sneaking
                    m_fpc.m_movingState = MovingState.Walk;
                }
                else
                {
                    // We came from walking / running
                    m_fpc.m_movingState = MovingState.Sneak;
                }

                // Finally set the new is Up state
                m_isUp = isUp;
            }
        }

        /// <summary>
        /// Updates the height of the first person controller
        /// </summary>
        /// <param name="currentPos">The current position of the vr node</param>
        private void UpdateHeight(Vector3 currentPos)
        {
            // Calculate the difference in an 0-1 range
            float t = (currentPos.y - m_downPosition.y) / m_heightDifference;
            // Lerp the height
            m_fpc.LerpHeight(t);
        }
        #endregion // Input

        #region Calibration
        /// <summary>
        /// Start calibration
        /// </summary>
        public void Calibrate()
        {
            var f = Input.GetAxis("CalibrateHeight");

            if (f == 0) { return; } // Nothing to calibrate
            else if (f > 0f) { CalibrateUp(); }
            else if (f < 0f) { CalibrateDown(); }
        }

        /// <summary>
        /// Calibrate what is considered "up"
        /// </summary>
        public void CalibrateUp()
        {
            // Get the position, that should be considered as "being up"
            m_upPosition = InputTracking.GetLocalPosition(m_vrNode);

            m_isCalibrated = true;
            m_heightDifference = m_upPosition.y - m_downPosition.y;
        }

        /// <summary>
        /// Calibrate what is considered "down"
        /// </summary>
        public void CalibrateDown()
        {
            // Get the position, that should be considered as "being down"
            m_downPosition = InputTracking.GetLocalPosition(m_vrNode);

            m_isCalibrated = true;
            m_heightDifference = m_upPosition.y - m_downPosition.y;
        }
        #endregion // Calibration
        #endregion // Methods
    }
}