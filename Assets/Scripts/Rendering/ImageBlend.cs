﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

namespace Master.Rendering.ImageEffects
{
    [ExecuteInEditMode]
    [AddComponentMenu("Image Effects/Custom/Image Blend")]
    public class ImageBlend : ImageEffectBase
    {
        #region Fields
        [SerializeField]
        public Texture m_texture;

        [SerializeField]
        public Texture m_alphaMap;

        [Range(0.0f, 1.0f)]
        public float m_intensity;
        #endregion // Fields

        // Called by camera to apply image effect
        void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            material.SetTexture("_Overlay", m_texture);
            material.SetTexture("_AlphaMap", m_alphaMap);
            material.SetFloat("_Intensity", m_intensity);
            Graphics.Blit(source, destination, material);
        }
    }
}