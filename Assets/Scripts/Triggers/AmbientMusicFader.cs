﻿using UnityEngine;
using System.Collections;

namespace Master.Triggers
{
    [RequireComponent(typeof(Collider))]
    public class AmbientMusicFader : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private AudioSource m_audio;
        [SerializeField] [Range(0f, 1f)]
        private float m_insideTriggerVolume = 1f;
        [SerializeField] [Range(0f, 1f)]
        private float m_outsideTriggerVolume = 0f;
        [SerializeField]
        private float m_volumeFadeTime = 4f;

        private bool m_insideTrigger = false;
        private float m_currentFadeTime = 0f;
        #endregion // Fields

        #region Methods
        /// <summary>
        /// Use this for initialization
        /// </summary>
        void Start()
        {
            // Get the audio source if it wasn't set inside the editor
            if (m_audio == null)
            {
                m_audio = GetComponent<AudioSource>();
            }
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        void Update()
        {
            UpdateVolume();
        }

        #region Volume
        private void UpdateVolume()
        {
            // Already faded
            if (m_currentFadeTime == 0f) { return; }

            // Reduce current fade time by dt
            m_currentFadeTime = Mathf.Max(0f, m_currentFadeTime - Time.deltaTime);

            // Get the values between which should be faded
            var sourceVolume = (m_insideTrigger ? m_outsideTriggerVolume : m_insideTriggerVolume);
            var targetVolume = (m_insideTrigger ? m_insideTriggerVolume : m_outsideTriggerVolume);

            // Calculate the value for lerping
            float fadeValue = 1f - (m_currentFadeTime / m_volumeFadeTime);

            // Set new volume by the lerped value
            m_audio.volume = Mathf.Lerp(sourceVolume, targetVolume, fadeValue);
        }
        #endregion // Volume

        #region Trigger
        private void OnTriggerEnter(Collider other)
        {
            // Only react on player
            if (!other.gameObject.CompareTag("Player")) { return; }

            // Player is inside the trigger
            m_insideTrigger = true;
            m_currentFadeTime = m_volumeFadeTime - m_currentFadeTime;
        }
        private void OnTriggerExit(Collider other)
        {
            // Only react on player
            if (!other.gameObject.CompareTag("Player")) { return; }

            // Player left the trigger
            m_insideTrigger = false;
            m_currentFadeTime = m_volumeFadeTime - m_currentFadeTime;
        }
        #endregion // Trigger
        #endregion // Methods
    }
}