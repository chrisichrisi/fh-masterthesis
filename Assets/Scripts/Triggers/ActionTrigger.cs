﻿using UnityEngine;
using System.Collections;

namespace Master.Trigger
{
    public class ActionTrigger : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private GameObject m_triggerResponder;

        private bool m_actionStarted = false;
        private bool m_isInside = false;

        private bool m_checkPredicateOnUpdate = true;
        #endregion // Fields

        #region Methods
        protected void Init()
        {
            if (m_triggerResponder == null)
            {
                m_triggerResponder = this.gameObject;
            }
        }

        void Start()
        {
            Init();
        }

        void Update()
        {
            if (!m_checkPredicateOnUpdate || !m_isInside) { return; }

            var predicate = CheckPredicate();
            if (m_actionStarted != predicate)
            {
                DoAction(predicate);
            }

        }

        void OnTriggerEnter(Collider other)
        {
            if (!other.gameObject.CompareTag("Player")) { return; }

            m_isInside = true;
            if (!CheckPredicate()) { return; }

            DoAction(true);
        }

        void OnTriggerExit(Collider other)
        {
            if (!other.gameObject.CompareTag("Player")) { return; }

            m_isInside = false;
            if (!m_actionStarted) { return; }

            DoAction(false);
        }

        void DoAction(bool start)
        {
            if (start) { m_triggerResponder.BroadcastMessage("StartAction"); }
            else { m_triggerResponder.BroadcastMessage("StopAction"); }

            m_actionStarted = start;
        }

        public virtual bool CheckPredicate() { return true; }
        #endregion // Methods
    }
}