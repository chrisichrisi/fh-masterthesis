﻿using UnityEngine;
using System.Collections;
using Master.Character.Sanity;

namespace Master.Trigger
{
    public class SanityActionTrigger : ActionTrigger
    {
        #region Fields
        private SanityMeter m_sanityMeter;

        [SerializeField]
        private float m_maximumSanity = 0.5f;
        #endregion // Fields

        #region Methods
        void Start()
        {
            base.Init();
            m_sanityMeter = GameObject.FindObjectOfType<SanityMeter>();
        }

        public override bool CheckPredicate()
        {
            return (m_sanityMeter.m_sanity <= m_maximumSanity);
        }
        #endregion // Methods
    }
}