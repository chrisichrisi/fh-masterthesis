﻿using UnityEngine;
using System.Collections;

namespace Master.Special
{
    [RequireComponent(typeof(Collider))]
    public class Pickup : MonoBehaviour
    {
        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                // TODO: Maybe play a little sound before
                gameObject.SetActive(false);
            }
        }
    }
}