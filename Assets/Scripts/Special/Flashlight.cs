﻿using UnityEngine;
using System.Collections;


namespace Master.Special
{
    public class Flashlight : MonoBehaviour
    {
        #region Fields
        private Light m_light;
        private AudioSource m_audio;
        Vector3 m_rot;

        [SerializeField]
        Vector2 m_minRotation = new Vector2(-30f, -20f);
        [SerializeField]
        Vector2 m_maxRotation = new Vector2(15f, 20f);

        private bool m_lightIsOn;
        private float m_lightIntensity;
        private float m_lightIntensityOn;

        #region Control
        [SerializeField]
        private AudioClip m_audioFlashlightOn;
        [SerializeField]
        private AudioClip m_audioFlashlightOff;
        #endregion // Control

        #region Disturbance
        private bool m_isDisturbed = false;
        private float m_disturbedTime = 0f;
        private float m_disturbanceRegenerateTime = 0f;
        private float m_disturbanceRegenerationTime = 5.0f;
        #endregion // Disturbance
        #endregion // Fields

        #region Methods
        /// <summary>
        /// Use this for initialization
        /// </summary>
        void Start()
        {
            m_light = GetComponent<Light>();
            m_audio = GetComponent<AudioSource>();

            m_rot = this.transform.localRotation.eulerAngles;
            // Set the start values
            m_lightIsOn = m_light.enabled;
            m_lightIntensity = m_light.intensity;
            m_lightIntensityOn = m_light.intensity;
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        void Update()
        {
            UpdateFlashlightDisturbance();

            if (m_lightIsOn)
            {
                UpdateFlashlightRotation();
            }

            // If the flashlight is currently disturbed, stop here
            if (m_isDisturbed) { return; }

            UpdateFlashlightState();
        }

        private void UpdateFlashlightState()
        {
            if (Input.GetButtonDown("FlashlightToggle"))
            {
                ToggleFlashlight();
            }
        }

        private void UpdateFlashlightRotation()
        {
            // If the flashlight modifier button is pressed, don't update the mouse look
            if (!Input.GetButton("FlashlightModifier")) { return; }

            // Read the input
            float yRot = Input.GetAxis("Mouse X") * 2.0f;
            float xRot = Input.GetAxis("Mouse Y") * 2.0f;

            // Calculate the new rotation
            Vector3 newRot = new Vector3(-xRot, yRot, 0f);
            m_rot += newRot;

            // Clamp the values
            m_rot.x = Mathf.Clamp(m_rot.x, m_minRotation.x, m_maxRotation.x);
            m_rot.y = Mathf.Clamp(m_rot.y, m_minRotation.y, m_maxRotation.y);

            // Set the new rotation
            this.transform.localRotation = Quaternion.Euler(m_rot);
        }

        #region Flashlight-Control
        public void ToggleFlashlight(bool state)
        {
            m_lightIsOn = state;
            m_light.enabled = m_lightIsOn;

            // Play audio
            if (m_lightIsOn)
            {
                m_audio.PlayOneShot(m_audioFlashlightOn);
            }
            else
            {
                m_audio.PlayOneShot(m_audioFlashlightOff);
            }
        }

        public void ToggleFlashlight() { ToggleFlashlight(!m_lightIsOn); }
        public void ActivateFlashlight() { ToggleFlashlight(true); }
        public void DeactivateFlashlight() { ToggleFlashlight(false); }
        #endregion // Flashlight-Control

        #region Flashlight-Disturbance
        private void UpdateFlashlightDisturbance()
        {
            if (m_isDisturbed)
            {
                if (m_disturbedTime > 0f)
                {
                    // Calculate a random intensity
                    m_lightIntensity = Random.Range(0f, m_lightIntensityOn);

                    // Randomly lerp the intensity
                    m_light.intensity = Mathf.Lerp(m_light.intensity, m_lightIntensity, Random.value);

                    // Reduce time
                    m_disturbedTime -= Time.deltaTime;
                    return;
                }

                if (m_light.intensity > 0.1f)
                {
                    // Lerp towards zero
                    m_light.intensity = Mathf.Lerp(m_light.intensity, 0f, Time.deltaTime * 2f);
                    return;
                }

                // Unset light intensity
                m_light.intensity = 0.0f;

                if (m_disturbanceRegenerateTime > 0f)
                {
                    // Reduce the time
                    m_disturbanceRegenerateTime -= Time.deltaTime;
                    return;
                }

                // Reset isDisturbed
                m_isDisturbed = false;
            }
            else
            {
                if (m_light.intensity < m_lightIntensityOn)
                {
                    // Reset intensity
                    m_light.intensity = Mathf.Lerp(m_light.intensity, m_lightIntensityOn, Time.deltaTime * 2f);
                }
            }
        }

        public void DisturbFlashlight()
        {
            m_isDisturbed = true;
            m_disturbedTime = 2.0f;
            m_disturbanceRegenerateTime = m_disturbanceRegenerationTime;
        }
        #endregion // Flashlight-Disturbance
        #endregion // Methods
    }
}