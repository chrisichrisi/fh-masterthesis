﻿using UnityEngine;
using System.Collections;

namespace Master.Special.Triggers
{
    [RequireComponent(typeof(Collider))]
    public class FlashlightDisturbanceTrigger : MonoBehaviour
    {
        #region Enums
        public enum FlashlightDisturbanceTriggerType
        {
            OneShot,
            MultiShot,
            Area
        }
        #endregion // Enums

        #region Fields
        [SerializeField]
        public FlashlightDisturbanceTriggerType m_type = FlashlightDisturbanceTriggerType.OneShot;

        private GameObject m_player = null;

        [SerializeField]
        public float m_sanityImpact = 0.05f;
        #endregion // Fields

        #region Methods
        /// <summary>
        /// Called every frame
        /// </summary>
        void Update()
        {
            if (m_type == FlashlightDisturbanceTriggerType.Area && m_player != null)
            {
                // Redistrub
                m_player.BroadcastMessage("DisturbFlashlight");
            }
        }

        /// <summary>
        /// Called, when a collider entered the trigger
        /// </summary>
        /// <param name="other">The collider that entered the trigger</param>
        void OnTriggerEnter(Collider other)
        {
            // Only collide with player
            if (!other.gameObject.CompareTag("Player")) { return; }

            // Collision with a player
            // Disturb the flashlight
            m_player = other.gameObject;
            m_player.BroadcastMessage("DisturbFlashlight");
            m_player.BroadcastMessage("DecreaseSanity", m_sanityImpact);

            // If this is a one shot trigger, deactivate the game object
            if (m_type == FlashlightDisturbanceTriggerType.OneShot)
            {
                gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// Called, when a collider leaved the trigger
        /// </summary>
        /// <param name="other">The collider that leaved the trigger</param>
        void OnTriggerExit(Collider other)
        {
            // Nothing to do in this case
            if (m_type != FlashlightDisturbanceTriggerType.Area) { return; }

            if (other.gameObject == m_player)
            {
                // Player left the trigger
                m_player = null;
            }
        }
        #endregion // Methods
    }
}