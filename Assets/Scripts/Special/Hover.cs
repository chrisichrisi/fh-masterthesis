﻿using UnityEngine;
using System.Collections;

namespace Master.Special
{
    public class Hover : MonoBehaviour
    {
        #region Fields
        /// <summary>
        /// The bodies on which to apply the force
        /// </summary>
        [SerializeField]
        private Rigidbody[] m_bodies;

        /// <summary>
        /// The force to apply
        /// </summary>
        [SerializeField]
        private Vector3 m_hoverForce = new Vector3(0f, 10f, 0f);

        /// <summary>
        /// True, if the force should be applied
        /// </summary>
        [SerializeField]
        private bool m_applyForce = false;
        #endregion // Fields


        #region Methods
        void FixedUpdate()
        {
            // Stop, if no force should be applied
            if (!m_applyForce) { return; }

            foreach(var body in m_bodies)
            { // Apply the force to every object
                if (body != null)
                {
                    body.AddForce(m_hoverForce);
                }
            }
        }

        public void StartAction()
        {
            m_applyForce = true;
        }

        public void StopAction()
        {
            m_applyForce = false;
        }
        #endregion // Methods
    }
}